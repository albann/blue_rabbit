<?php
get_header(); 
?>

    <section class="not-found-section">

        <div class="container">

            <div class="col-lg-12">

                <h1>The content you're looking for does not exist.</h1>

                <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Immo alio genere; Conferam tecum, quam cuique verso rem subicias; Quid est, quod ab ea absolvi et perfici debeat? Restinguet citius, si ardentem acceperit. Quamquam id quidem licebit iis existimare, qui legerint. Ut alios omittam, hunc appello, quem ille unum secutus est. Et quidem Arcesilas tuus, etsi fuit in disserendo pertinacior, tamen noster fuit; Duo Reges: constructio interrete.</p>

            </div>

        </div>

    </section>

<?php
get_footer(); 
?>