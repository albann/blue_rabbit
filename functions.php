<?php


//Include bootstrap nav walker
require_once('includes/wp_bootstrap_navwalker.php');

function html24_theme_setup() {

	//Add Custom Logo Support
	add_theme_support( 'custom-logo', array(
		'height'      => 110,
		'width'       => 200,
		'flex-height' => true
	) );

	//<title> support
	add_theme_support( 'title-tag' );

	//Thumbnails in posts
	add_theme_support( 'post-thumbnails' );

	//Woocommerce support
	add_theme_support( 'woocommerce' );


	// Register main menu
	register_nav_menus( array(
		'main' => 'Main Menu',
	) );

}


add_action( 'after_setup_theme', 'html24_theme_setup' );


function html24_widgets() {

	register_sidebar( array(
		'name'          => 'Footer',
		'description' 	=> 'Footer Widget',
		'id'            => 'footer-widget',
		'before_widget' => '<div id="%1$s" class="%2$s col-lg-4 col-xs-12">',
		'after_widget'  => '</div>',
		'before_title'  => '<h3>',
		'after_title'   => '</h3>',
	) );

}
add_action( 'widgets_init', 'html24_widgets' );




/**
 * Register and load any scripts your themes frontend requires.
 */
add_action('wp_enqueue_scripts', function () {

	wp_enqueue_script( 'events', get_template_directory_uri() . '/js/frontend/base/events.js', array(), 1.1, true );

	wp_enqueue_script( 'wrapper', get_template_directory_uri() . '/js/frontend/base/wrapper.js', array (), 1.1, true);

	wp_enqueue_script( 'heroslider', get_template_directory_uri() . '/js/frontend/modules/heroslider.js', array (), 1.1, true);

	wp_enqueue_script('google-map', get_template_directory_uri() . '/js/frontend/modules/map.js', false, 1.1, true);

    wp_enqueue_script('google-map-api', 'https://maps.googleapis.com/maps/api/js?key=AIzaSyB5rL9EncVBEV5s3rmVvB6zTRmgo5bgNSw', [], 1.1, true);

	wp_enqueue_script( 'search-form', get_template_directory_uri() . '/js/frontend/modules/search-form.js', array ( ), 1.1, true);

	wp_enqueue_script( 'ajax-js', get_template_directory_uri() . '/js/shared/helpers/ajax.js', array (), 1.1, true);

	wp_enqueue_script( 'bootstrap-min-js', get_template_directory_uri() . '/includes/bootstrap.min.js', array ( 'jquery' ), 1.1, true);

	wp_enqueue_script( 'main-js', get_template_directory_uri() . '/js/main.js', array (), 1.1, true);

	wp_enqueue_script( 'jquery-bxslider-min-js', get_template_directory_uri() . '/includes/jquery.bxslider.min.js', array ( 'jquery' ), 1.1, true);


});

/**
 * Register and load any stylesheets your themes frontend requires.
 */
add_action('wp_enqueue_scripts', function () {

    wp_enqueue_style( 'bootstrap-css', get_template_directory_uri() . "/css/bootstrap.min.css");

    wp_enqueue_style( 'main-css', get_template_directory_uri() . "/css/main.css");

});