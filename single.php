<?php

get_header();

?>

<section class="default-text-section row">
<div class="container">

    <!-- Do we have posts -->
<?php if ( have_posts() ): ?>
    <!-- Yes we have posts -->
 <?php while ( have_posts() ): the_post(); ?>
    <?php get_template_part('template-parts/content', 'single'); ?>

 <?php endwhile; ?>

 <?php else: ?>
    <?php get_template_part('template-parts/content', 'none'); ?>
<?php endif; ?>

    

</div>
</section>

<?php
get_footer();

    