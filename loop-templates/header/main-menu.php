<?php wp_nav_menu( array(
	'theme_location'	=> 'main',
	'menu_id' 			=> 'main-menu',
	'depth' 			=> 2, // 1 = no dropdowns, 2 = with dropdowns.
	'container_class' 	=> 'collapse navbar-collapse',
	'container_id' 		=> 'navbar',
	'menu_class'		=> 'nav navbar-nav navbar-right',
	'fallback_cb' 		=> 'WP_Bootstrap_Navwalker::fallback',
	'walker' 			=> new WP_Bootstrap_Navwalker()
) );