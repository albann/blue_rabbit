<?php
get_header(); ?>

<section class="default-text-section row">

        <div class="container">

            <?php
                while ( have_posts() ) : the_post();

                    get_template_part( 'loop-templates/content', 'page' );

                endwhile; // End of the loop.
                ?>

    </div>
</section>

<?php
get_footer(); 
?>