
<form role="search" method="get" class="navbar-form search-form" action="<?php echo esc_url( home_url( '/' ) ); ?>">
	<div class="form-group search-input-container">
		<input type="search" class="form-control search-input" placeholder="<?php echo esc_attr_x( 'Search &hellip;', 'placeholder', 'blue_rabbit' ); ?>" value="<?php echo get_search_query(); ?>" name="s" />
	</div>
	<button type="submit" class="btn btn-default"><span class="glyphicon glyphicon-search"></span></button>
</form>

