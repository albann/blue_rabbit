<!DOCTYPE html>
<html lang="da">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <?php wp_head(); ?>
</head>
<body <?php body_class(); ?>>
<div class="main-wrap container-fluid">
    <div class="top-bar row">
        <!-- Search Bar -->
            <div class="container">
                <div class="row">
                    <?php get_search_form( true ); ?>
            </div>
        </div>
    </div>
    <header class="top">
        <!-- Blue Logo and Main Menu -->
        <?php get_template_part( 'loop-templates/header/header', 'top' ); ?>       
    </header>