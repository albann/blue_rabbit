<?php get_header(); ?>


<section class="hero-slider-section row">
	<ul class="hero-slider">

	<?php while( have_rows('slider') ): the_row(); 
		// vars
		$image = get_sub_field('image');
		?>

		<li class="slide">
			<img src="<?php echo $image['url']; ?>" alt="<?php echo $image['alt'] ?>" />
		</li>

	<?php endwhile; ?>

	</ul>
</section>


<section class="quick-links-section">

    <?php 
    if (have_posts()) {
  		while (have_posts()) {
    		the_post();
    		the_content(); 
  			}
		}	 
	?>

</section>

<?php get_footer();