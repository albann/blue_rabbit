<?php 
/**
 * 
 * This will override single-product.php and archive-product.php
 * 
 */
?>

<?php get_header(); ?>
    <section class="breadcrumb-wrap row clearfix">
        <div class="container">
            <?php woocommerce_breadcrumb(); ?>
        </div>
    </section>

    <section class="products-section">
        <div class="container">
            <?php woocommerce_content(); ?>        
        </div>
    </section>
<?php get_footer();