<div class="row">
	<div class="container">
		<div class="row">
			<div class="col-lg-6">
				<div class="logo-wrap pull-left">
					<?php the_custom_logo(); ?>
				</div>
			</div>
			<div class="col-lg-6 navigation-menu">
				<?php get_template_part( 'loop-templates/header/main', 'menu' ); ?>
			</div>
		</div>
	</div>
</div>