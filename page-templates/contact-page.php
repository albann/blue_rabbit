<?php
/**
 * Template Name: Contact Page
 */
?>

<?php get_header(); ?>


<section class="default-page-section default-text-section row">

    <div class="container">
        <div class="row">

            <div id="map" style="height:500px;"></div>

            <?php
                while ( have_posts() ) : the_post();
                    get_template_part( 'loop-templates/content', 'page' );
                endwhile;
                ?>

            </div>
        </div>

    </div>

</section>
<?php get_footer();