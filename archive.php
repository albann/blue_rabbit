<?php
get_header();
?>

<section class="post-archive-section">

        <div class="row">

            <div class="container">

                <div class="row">


                <?php if ( have_posts() ) : ?>

                    <header class="page-header">
                        <?php
                        the_archive_title( '<h1 class="page-title">', '</h1>' );
                        the_archive_description( '<div class="taxonomy-description">', '</div>' );
                        ?>
                    </header><!-- .page-header -->

                    <?php /* Start the Loop */ ?>
                    <?php while ( have_posts() ) : the_post(); ?>

                        <?php

                        /*
                         * Include the Post-Format-specific template for the content.
                         * If you want to override this in a child theme, then include a file
                         * called content-___.php (where ___ is the Post Format name) and that will be used instead.
                         */
                        get_template_part( 'loop-templates/content', get_post_format() );
                        ?>

                    <?php endwhile; ?>

                <?php else : ?>

                    <?php get_template_part( 'loop-templates/content', 'none' ); ?>

                <?php endif; ?>

            </div><!-- #row -->

            <!-- The pagination component -->
            <?php the_posts_pagination(); ?>

        </div> <!-- .container -->

      </div> <!-- .row -->

    </section><!-- #section -->

<?php get_footer();
