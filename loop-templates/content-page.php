<article <?php post_class(); ?> id="post-<?php the_ID(); ?>">


	<div class="entry-content">

		<?php the_content(); ?>


	</div><!-- .entry-content -->



	<div class="entry-footer">
		<?php edit_post_link(); ?>
	</div>

</article><!-- #post-## -->